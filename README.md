# The Manned.org Source Code

This repository holds the source code of Manned.org. For a description of the
site, check out [https://manned.org/info/about](https://manned.org/info/about).
  
Ironically, documentation about how things work is completely lacking.

## Requirements

- perl: A somewhat recent version (no idea which, due to my XS usage)
- postgresql: Also a somewhat recent version
- rust: Version who-knows-which

### Web front-end

- AnyEvent
- DBD::Pg
- DBI
- JSON::XS
- SQL::Interp
- TUWF

### Man page indexer

- curl
- psql

## File structure

- **indexer/** -> The Rust program that scans package repositories for updates, fetches new packages and extracts the man pages.
- **lib/ManUtils/** -> Perl/XS helper module to format man pages into HTML (uses **web/**).
- **sql/** -> Database schema & updates.
- **util/** -> Cron job and scripts to run **indexer/** on the right repositories.
- **web/** -> Badly named Rust library to convert man pages into HTML.
- **www/** -> The web front-end.
