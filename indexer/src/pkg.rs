use std;
use std::io::{Error,ErrorKind,Read};
use postgres;
use chrono::NaiveDateTime;

use crate::open;
use crate::archread;
use crate::man;
use crate::archive::{Format,Archive,ArchiveEntry};

pub static mut DRY_RUN: bool = false;


#[derive(Debug,Clone,Copy)]
pub enum Date<'a> {
    Known(&'a str), // Given in PkgOpt
    Found(i64),     // Found in package
    Deb,            // Should be read from the timestamp of the 'debian-binary' file
    Desc,           // Should be read from the '+DESC' file (FreeBSD <= 9.2)
    Max,            // Use the latest timestamp in the archive
    MaxVal(i64),
}


impl<'a> Date<'a> {
    fn update(&mut self, ent: &ArchiveEntry) {
        // TODO: Validate that the mtime() date is sensible (e.g. 1990 < date < now)
        *self = match *self {
            Date::Deb if ent.format() == Format::Ar && ent.path() == Some("debian-binary") => Date::Found(ent.mtime()),
            Date::Desc if ent.path() == Some("+DESC") => Date::Found(ent.mtime()),
            Date::Max => Date::MaxVal(ent.mtime()),
            Date::MaxVal(t) if ent.mtime() > t => Date::MaxVal(ent.mtime()),
            x => x,
        }
    }
}


pub struct PkgOpt<'a> {
    pub force: bool,
    pub sys: i32,
    pub cat: &'a str,
    pub pkg: &'a str,
    pub ver: &'a str,
    pub date: Date<'a>,
    pub arch: Option<&'a str>,
    pub file: open::Path<'a>
}


fn insert_pkg(tr: &mut postgres::Transaction, opt: &PkgOpt) -> Option<i32> {
    let pkginfo = format!("sys {} / {} / {} - {} @ {:?} @ {}", opt.sys, opt.cat, opt.pkg, opt.ver, opt.date, opt.file.path);

    // Use a custom CTE-based insert-or-update. Using an INSERT with an ON CONFLICT clause would be
    // easier, but has the downside of allocating a new package id even if one already exists.
    // The separate UPDATE query makes sure to unflag the package as dead while not causing any
    // database writes when the row's already fine.
    let q = "WITH p(id) AS (SELECT id FROM packages WHERE system = $1 AND category = $2 AND name = $3),
                  u     AS (UPDATE packages SET dead = FALSE FROM p WHERE packages.id = p.id AND dead),
                  i(id) AS (INSERT INTO packages (system, category, name) SELECT $1, $2, $3 WHERE NOT EXISTS(SELECT 1 FROM p) RETURNING id)
             SELECT id FROM p UNION SELECT id FROM i";
    let pkgid: i32 = match tr.query_one(q, &[&opt.sys, &opt.cat, &opt.pkg]) {
        Err(e) => {
            error!("Can't insert package in database: {}", e);
            return None;
        },
        Ok(r) => r.get(0),
    };

    let q = "SELECT id FROM package_versions WHERE package = $1 AND version = $2";
    let res = tr.query_opt(q, &[&pkgid, &opt.ver]).unwrap();

    let verid : i32;

    let date = match opt.date {
        Date::Known(d) => d,
        _ => "1980-01-01", // Placeholder
    };

    if res.is_none() {
        let q = "INSERT INTO package_versions (package, version, released, arch) VALUES($1, $2, $3::text::date, $4) RETURNING id";
        verid = tr.query_one(q, &[&pkgid, &opt.ver, &date, &opt.arch]).unwrap().get(0);
        info!("New package pkgid {} verid {}, {}", pkgid, verid, pkginfo);
        Some(verid)

    } else if opt.force {
        // XXX: Should we update released & arch here?
        verid = res?.get(0);
        info!("Overwriting package pkgid {} verid {}, {}", pkgid, verid, pkginfo);
        tr.query("DELETE FROM files WHERE pkgver = $1", &[&verid]).unwrap();
        Some(verid)

    } else {
        debug!("Package already in database, pkgid {} verid {}, {}", pkgid, res?.get::<usize,i32>(0), pkginfo);
        None
    }
}


fn insert_man_row<T: postgres::GenericClient>(tr: &mut T, verid: i32, path: &str, enc: &str, content: i32) {
    let (name, sect, locale) = man::parse_path(path).unwrap();
    let q = "WITH ms(id) AS (SELECT id FROM mans WHERE name = $2 AND section = $3),
                  mi(id) AS (INSERT INTO mans (name, section) SELECT $2, $3 WHERE NOT EXISTS(SELECT 1 FROM ms) RETURNING id),
                  m(id) AS (SELECT id FROM ms UNION SELECT id FROM mi),
                  ls(id) AS (SELECT id FROM locales WHERE locale = $5),
                  li(id) AS (INSERT INTO locales (locale) SELECT $5 WHERE NOT EXISTS(SELECT 1 FROM ls) RETURNING id),
                  l(id) AS (SELECT id FROM ls UNION SELECT id FROM li),
                  es(id) AS (SELECT id FROM encodings WHERE encoding = $6),
                  ei(id) AS (INSERT INTO encodings (encoding) SELECT $6 WHERE NOT EXISTS(SELECT 1 FROM es) RETURNING id),
                  e(id) AS (SELECT id FROM es UNION SELECT id FROM ei),
                  c(shorthash) AS (SELECT hash_to_shorthash(hash) FROM contents WHERE id = $4)
             INSERT INTO files (pkgver, man, content, shorthash, locale, encoding, filename)
             SELECT $1, m.id, $4, c.shorthash, l.id, e.id, '/'||$7 FROM m, l, e, c";
    if let Err(e) = tr.execute(q, &[&verid, &name, &sect, &content, &locale, &enc, &path]) {
        // I think this can only happen if archread gives us the same file twice, which really
        // shouldn't happen. But I'd rather continue with an error logged than panic.
        error!("Can't insert verid {} fn {}: {}", verid, path, e);
    }
}


fn insert_man<T: postgres::GenericClient>(tr: &mut T, verid: i32, paths: &[&str], ent: &mut dyn Read) {
    let (dig, enc, mut cont) = match man::decode(paths, ent) {
        Err(e) => { error!("Error decoding {}: {}", paths[0], e); return },
        Ok(x) => x,
    };

    // Postgres doesn't like the 0-byte in UTF-8 (and rightly so).
    if cont.contains(0 as char) {
        warn!("Removing 0-byte in man page contents");
        cont = cont.replace(0 as char, "");
    }

    let q = "WITH s(id) AS (SELECT id FROM contents WHERE hash = $1),
                  i(id) AS (INSERT INTO contents (hash, content) SELECT $1, $2 WHERE NOT EXISTS(SELECT 1 FROM s) RETURNING id)
             SELECT id FROM s UNION SELECT id FROM i";
    let id: i32 = tr.query_one(q, &[&dig.as_ref(), &cont]).unwrap().get(0);

    for path in paths {
        insert_man_row(tr, verid, path, enc, id);
        info!("Inserted man page: {} ({})", path, enc);
    }
}


fn insert_link<T>(tr: &mut T, verid: i32, src: &str, dest: &str) where T: postgres::GenericClient {
    let q = "SELECT f.content, e.encoding FROM files f JOIN encodings e ON e.id = f.encoding WHERE pkgver = $1 AND filename = '/'||$2";
    let res = match tr.query_opt(q, &[&verid, &dest]).unwrap() {
        None => { /* Can happen if man::decode() failed previously. */
            error!("Link to unindexed man page: {} -> {}", src, dest);
            return;
        },
        Some(x) => x
    };
    let content: i32 = res.get(0);
    let enc: String = res.get(1);
    insert_man_row(tr, verid, src, &enc, content);
    info!("Inserted man link: {} -> {}", src, dest);
}


fn with_pkg<F,T>(opt: &mut PkgOpt, cb: F) -> std::io::Result<T>
    where F: FnOnce(Option<ArchiveEntry>, &mut PkgOpt) -> std::io::Result<T>
{
    let mut rd = opt.file.open()?;
    let ent = match Archive::open_archive(&mut rd)? {
        None => return cb(None, opt),
        Some(x) => x,
    };

    // .deb ("2.0")
    if ent.format() == Format::Ar && ent.path() == Some("debian-binary") {
        opt.date.update(&ent);
        let mut ent = ent.next()?;
        while let Some(mut e) = ent {
            opt.date.update(&e);
            if e.path().map(|p| p.starts_with("data.tar")) == Some(true) {
                return cb(Archive::open_archive(&mut e)?, opt);
            }
            ent = e.next()?
        }
        Err(Error::new(ErrorKind::Other, "Debian file without data.tar"))

    // any other archive (Arch/FreeBSD .tar)
    } else {
        cb(Some(ent), opt)
    }
}


fn index_pkg<T: postgres::GenericClient>(tr: &mut T, mut opt: PkgOpt, verid: i32) -> std::io::Result<()> {
    let missed = with_pkg(&mut opt, |e, opt| {
            archread::FileList::read(e, man::ismanpath, |ent| opt.date.update(ent), |paths, ent| {
                insert_man(tr, verid, paths, ent);
                Ok(())
            })
        })?.links(|src, dest| { insert_link(tr, verid, src, dest) });

    if let Some(missed) = missed {
        warn!("Some links were missed, reading package again");
        with_pkg(&mut opt, |e, _| { missed.read(e, |paths, ent| {
            insert_man(tr, verid, paths, ent);
            Ok(())
        }) })?
    }

    match opt.date {
        Date::Known(_) => Ok(()),
        Date::Found(t) | Date::MaxVal(t) => {
            let date = NaiveDateTime::from_timestamp(t, 0).format("%Y-%m-%d").to_string();
            debug!("Date from package: {}", date);
            tr.execute("UPDATE package_versions SET released = $1::text::date WHERE id = $2", &[&date, &verid]).unwrap();
            Ok(())
        },
        _ => Err(Error::new(ErrorKind::Other, "No valid date found in this package")),
    }
}


pub fn pkg<T>(conn: &mut T, opt: PkgOpt) where T: postgres::GenericClient {
    let mut tr = conn.transaction().unwrap();

    let verid = match insert_pkg(&mut tr, &opt) { Some(x) => x, None => return };
    if unsafe { DRY_RUN } {
        return;
    }

    if let Err(e) = index_pkg(&mut tr, opt, verid) {
        error!("Error reading package: {}", e);
        return;
    }

    if let Err(e) = tr.commit() {
        error!("Error finishing transaction: {}", e);
    }
}
