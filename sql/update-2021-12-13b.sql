ALTER TABLE packages
    ALTER COLUMN category SET NOT NULL,
    ADD COLUMN dead     boolean   NOT NULL DEFAULT FALSE;
