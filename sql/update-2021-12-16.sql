-- 'overrides' is not a valid locale.  Rather, that was a directory that CentOS
-- used for updated man pages without overwriting the pages already installed
-- on the system.
-- 'man' is just a badly nested directory.
-- The others are openmpi using non-standard directories for some reason.
WITH b(id) AS (SELECT id FROM locales WHERE locale IN('man', 'overrides') OR locale ~ '(openmpi|mpich|mvapich)'),
     f     AS (UPDATE files SET locale = 0 WHERE locale IN(SELECT id FROM b))
DELETE FROM locales WHERE id IN(SELECT id FROM b);
