#!/bin/sh

. ./common.sh

MIRROR=http://dl-cdn.alpinelinux.org/alpine/

alp() {
    local VER=$1
    local REPOS=$2

    for REPO in $REPOS; do
        index alpine --sys alpine-$VER --mirror ${MIRROR}v$VER --repo $REPO
    done
}

case "$1" in
    3.0)
        alp 3.0 main
        ;;
    3.1)
        alp 3.1 main
        ;;
    3.2)
        alp 3.2 main
        ;;
    3.3)
        alp 3.3 "main community"
        ;;
    3.4)
        alp 3.4 "main community"
        ;;
    3.5)
        alp 3.5 "main community"
        ;;
    3.6)
        alp 3.6 "main community"
        ;;
    3.7)
        alp 3.7 "main community"
        ;;
    3.8)
        alp 3.8 "main community"
        ;;
    3.9)
        alp 3.9 "main community"
        ;;
    3.10)
        alp 3.10 "main community"
        ;;
    3.11)
        alp 3.11 "main community"
        ;;
    3.12)
        alp 3.12 "main community"
        ;;
    3.13)
        alp 3.13 "main community"
        ;;
    3.14)
        alp 3.14 "main community"
        ;;
    3.15)
        alp 3.15 "main community"
        ;;
    3.16)
        alp 3.16 "main community"
        ;;
    old)
        $0 3.0
        $0 3.1
        $0 3.2
        $0 3.3
        $0 3.4
        $0 3.5
        $0 3.6
        $0 3.7
        $0 3.8
        $0 3.9
        $0 3.10
        $0 3.11
        $0 3.12
        ;;
    current)
        $0 3.13 # till 2022-11-01
        $0 3.14 # till 2023-05-01
        $0 3.15 # till 2023-11-01
        $0 3.16 # till 2024-05-23
        ;;
    all)
        $0 old
        $0 current
        ;;
esac
