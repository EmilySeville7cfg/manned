-- Create a new table before replacing in order to avoid a long-held lock on
-- the table being replaced. The site should remain responsive while these
-- queries are run.
BEGIN;
CREATE TABLE stats_cache_new AS
    SELECT (SELECT count(*) FROM contents) AS hashes,
           (SELECT count(distinct name) FROM mans) AS mans, *
      FROM (SELECT count(*), count(distinct pkgver) FROM files) x(files, packages);
DROP TABLE stats_cache;
ALTER TABLE stats_cache_new RENAME TO stats_cache;
COMMIT;
